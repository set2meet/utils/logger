'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var socket_ioClient = require('socket.io-client');
var safeJsonStringify = require('safe-json-stringify');
var stacktraceJs = require('stacktrace-js');
var handlebars = require('handlebars');
var socket_io = require('socket.io');
var os = require('os');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var safeJsonStringify__default = /*#__PURE__*/_interopDefaultLegacy(safeJsonStringify);

(function(S2MServices2) {
  S2MServices2["VirtualRoomClient"] = "VirtualRoomClient";
  S2MServices2["VirtualRoomServer"] = "VirtualRoomServer";
  S2MServices2["RecordingCapture"] = "RecordingCapture";
  S2MServices2["RecordingDb"] = "RecordingDb";
  S2MServices2["RecordingProcessing"] = "RecordingProcessing";
  S2MServices2["TestingPlatform"] = "TestingPlatform";
})(exports.S2MServices || (exports.S2MServices = {}));

var TransportEvent;
(function(TransportEvent2) {
  TransportEvent2["LOG"] = "log";
})(TransportEvent || (TransportEvent = {}));

class JSONSerializer {
  serialize(data) {
    return this.serializeToJSON(data);
  }
  deserialize(serializedData) {
    return JSON.parse(serializedData);
  }
  serializeToJSON(data) {
    try {
      return JSON.stringify(data);
    } catch (firstException) {
      try {
        const seen = new Set();
        return JSON.stringify(data, (key, value) => {
          if (!value || typeof value !== "object")
            return value;
          if (seen.has(value))
            return "[Circular]";
          seen.add(value);
          return value;
        });
      } catch (secondException) {
        return safeJsonStringify__default['default'](data);
      }
    }
  }
}

var __assign = Object.assign;
const ServerSyncTransport2 = class {
  constructor() {
    this.BATCH_DELAY = 3e3;
    this.logStackMap = {};
    this.isInitialized = false;
    this.serializer = new JSONSerializer();
    this.activeBatchOperation = null;
    this.onDisconnect = () => {
      this.isInitialized = false;
    };
  }
  dumpLogs(applyDeduplication = true) {
    this.applyForModuleStack((currentModule, currentStack) => {
      if (currentStack.length) {
        const dump = (applyDeduplication ? currentStack.filter((value, index, stack) => stack.indexOf(value) === index) : currentStack).join(ServerSyncTransport2.BATCH_SEPARATOR);
        if (dump) {
          this.sendLogs(dump);
        }
        currentStack.length = 0;
      }
    });
  }
  applyForModuleStack(action) {
    Object.keys(this.logStackMap).forEach((currentModule) => action.call(this, currentModule, this.logStackMap[currentModule]));
  }
  compress(message) {
    return message;
  }
  packLogs(message) {
    return this.compress(this.serializer.serialize(message));
  }
  setup(config, logger) {
    const {clientFactory, uri} = config;
    this.client = clientFactory ? clientFactory(config) : socket_ioClient.io(uri, __assign({
      reconnectionDelayMax: 1e3,
      timeout: 3e3
    }, config));
    this.client.on("connect", (serverSocket) => {
      this.isInitialized = true;
      this.dumpLogs();
      this.client.on("disconnect", this.onDisconnect);
    });
    this.client.on("connect_timeout", (timeout) => {
      logger.warn("connect_timeout", {timeout});
    });
    this.client.on("connect_error", (error) => {
      logger.error(error, "socket connect error");
    });
    this.client.on("error", (error) => {
      logger.error(error, "socket error");
    });
    this.client.on("disconnect", (reason) => {
      logger.info("Socket disconnected", {reason});
    });
  }
  destroy() {
    this.client.disconnect();
    this.logStackMap = {};
  }
  getLogStackByModule(module) {
    const moduleKey = module.toString();
    let resultStack = this.logStackMap[moduleKey];
    if (!resultStack) {
      this.logStackMap[moduleKey] = resultStack = [];
    }
    return resultStack;
  }
  sendLogs(message) {
    this.client.emit(TransportEvent.LOG, message);
  }
  log(message, applyBatch = true, applyDeduplication = true) {
    const rawMessage = message;
    if (applyBatch || !this.isInitialized) {
      this.getLogStackByModule(message.module).push(this.packLogs(rawMessage));
      if (!this.activeBatchOperation && this.isInitialized) {
        this.activeBatchOperation = window.setTimeout(() => {
          this.dumpLogs(applyDeduplication);
          this.activeBatchOperation = null;
        }, this.BATCH_DELAY);
      }
    } else {
      this.sendLogs(this.packLogs(rawMessage));
    }
  }
};
let ServerSyncTransport = ServerSyncTransport2;
ServerSyncTransport.BATCH_SEPARATOR = "_$_";

var LogLevel;
(function(LogLevel2) {
  LogLevel2[LogLevel2["FATAL"] = 0] = "FATAL";
  LogLevel2[LogLevel2["ERROR"] = 1] = "ERROR";
  LogLevel2[LogLevel2["WARN"] = 2] = "WARN";
  LogLevel2[LogLevel2["INFO"] = 3] = "INFO";
  LogLevel2[LogLevel2["DEBUG"] = 4] = "DEBUG";
  LogLevel2[LogLevel2["EVENT"] = 5] = "EVENT";
})(LogLevel || (LogLevel = {}));

(function(DefaultModules2) {
  DefaultModules2["DefaultModule"] = "App";
})(exports.DefaultModules || (exports.DefaultModules = {}));

class ConsoleTransport {
  log(message) {
    console.log(message);
  }
}

const isRecordEmpty = (record) => {
  for (const i in record)
    return false;
  return true;
};

var __assign$1 = Object.assign;
const CALLSTACK_LOG_TYPE = "callstack_log_type";
class BaseLogger {
  constructor(service, module = exports.DefaultModules.DefaultModule, settings = {}, boundFields, parent = null, transport = new ConsoleTransport()) {
    this.service = service;
    this.module = module;
    this.settings = settings;
    this.boundFields = boundFields;
    this.parent = parent;
    this.transport = transport;
    this.moduleContext = this.getModuleContext();
  }
  log(message, data, level = LogLevel.INFO, type = CALLSTACK_LOG_TYPE) {
    const {devMode, parseTemplateMessages} = this.settings;
    if (devMode && type === CALLSTACK_LOG_TYPE) {
      const stackTrace = this.getStackTrace();
      type = stackTrace.find((value) => value.functionName.indexOf(this.constructor.name) !== 0).functionName;
    }
    const logMeta = __assign$1({
      data: this.boundFields || data ? __assign$1(__assign$1({}, this.boundFields), data) : void 0,
      type,
      level
    }, this.calculateAdditionalFields());
    if (parseTemplateMessages && message) {
      message = handlebars.compile(message)(logMeta);
    }
    this.transport.log(__assign$1(__assign$1({}, logMeta), {message}));
  }
  calculateAdditionalFields() {
    return {
      service: this.service,
      module: this.module || exports.DefaultModules.DefaultModule,
      moduleContext: this.moduleContext.join("."),
      time: new Date().toISOString()
    };
  }
  getModuleContext() {
    const result = [this.getModule()];
    let currentNode = this.getParentLogger();
    while (currentNode) {
      result.push(currentNode.getModule());
      currentNode = currentNode.getParentLogger();
    }
    return result.reverse();
  }
  getModule() {
    return this.module;
  }
  getParentLogger() {
    return this.parent;
  }
  error(code, message, data) {
    this.logError(LogLevel.ERROR, code, message, data);
  }
  fatal(code, message, data) {
    this.logError(LogLevel.FATAL, code, message, data);
  }
  logError(logLevel, code, message, data) {
    let logData = data || {};
    let logMessage;
    if (typeof message !== "string") {
      logData = __assign$1(__assign$1({}, logData), message);
    } else {
      logMessage = message;
    }
    if (typeof code === "string" || typeof code === "number") {
      logData.code = code;
      if (!logMessage)
        logMessage = logData.code.toString();
    } else {
      const error = code;
      logData.error = code;
      logData.code = error.code;
      if (!logMessage)
        logMessage = error.message || error.code.toString();
    }
    this.log(logMessage, isRecordEmpty(logData) ? void 0 : logData, logLevel);
  }
  warn(message, data) {
    this.log(message, data, LogLevel.WARN);
  }
  info(message, data) {
    this.log(message, data, LogLevel.INFO);
  }
  debug(message, data) {
    this.log(message, data, LogLevel.DEBUG);
  }
  report(event, data) {
    this.log(void 0, data, LogLevel.EVENT, event.toString());
  }
  createBoundChild(module, boundFields) {
    return new BaseLogger(this.service, module, this.settings, this.boundFields || boundFields ? __assign$1(__assign$1({}, this.boundFields), boundFields) : void 0, this, this.transport);
  }
  getStackTrace() {
    return stacktraceJs.getSync();
  }
  destroy() {
  }
}

var __assign$2 = Object.assign;
class ClientLogger extends BaseLogger {
  constructor(service, module = exports.DefaultModules.DefaultModule, settings = {}, boundFields = void 0, parent = void 0) {
    super(service, module, settings, boundFields, parent, new ServerSyncTransport());
    this.service = service;
    this.module = module;
    this.settings = settings;
    this.boundFields = boundFields;
    this.parent = parent;
  }
  startToSync(config) {
    this.transport.setup(config, this);
  }
  calculateAdditionalFields() {
    return __assign$2({
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }, super.calculateAdditionalFields());
  }
  destroy() {
    super.destroy();
    this.transport.destroy();
  }
}

class StdOutTransport {
  destroy() {
  }
  rawLog(message) {
    this.log(new JSONSerializer().deserialize(message));
  }
  log(message) {
    console.log(message);
  }
  setup(config) {
  }
}

var SocketDisconnectReason;
(function(SocketDisconnectReason2) {
  SocketDisconnectReason2["ServerDisconnect"] = "io server disconnect";
  SocketDisconnectReason2["V2ServerDisconnect"] = "server namespace disconnect";
  SocketDisconnectReason2["ClientDisconnect"] = "io client disconnect";
  SocketDisconnectReason2["V2ClientDisconnect"] = "client namespace disconnect";
  SocketDisconnectReason2["PingTimeout"] = "ping timeout";
  SocketDisconnectReason2["TransportClose"] = "transport close";
  SocketDisconnectReason2["TransportError"] = "transport error";
})(SocketDisconnectReason || (SocketDisconnectReason = {}));

var __assign$3 = Object.assign;
const CONNECTION_EVENT = "connection";
const ServerLogger2 = class extends BaseLogger {
  constructor(service, module = exports.DefaultModules.DefaultModule, settings = {}, boundFields = void 0, parent = null) {
    super(service, module, settings, boundFields, parent, new StdOutTransport());
    this.service = service;
    this.module = module;
    this.settings = settings;
    this.boundFields = boundFields;
    this.parent = parent;
    this.systemInfo = {
      hostname: os.hostname(),
      pid: process.pid.toString()
    };
  }
  startToSync(serverConfig) {
    const config = __assign$3(__assign$3({}, ServerLogger2.DEFAULT_SERVER_CONFIG), serverConfig);
    const {serverFactory, port} = serverConfig;
    this.server = serverFactory ? serverFactory(config) : new socket_io.Server(+port, config);
    this.server.on(CONNECTION_EVENT, (clientSocket) => {
      clientSocket.on(TransportEvent.LOG, (message) => {
        const backendTransport = this.transport;
        const serializer = new JSONSerializer();
        this.decompressLogDump(message).map((value) => {
          return serializer.deserialize(value);
        }).sort((a, b) => {
          const date1 = new Date(a.time).getMilliseconds();
          const date2 = new Date(b.time).getMilliseconds();
          if (date1 > date2) {
            return 1;
          }
          if (date1 < date2) {
            return -1;
          }
          return 0;
        }).forEach((value) => backendTransport.log(value));
      });
      const internalChildLogger = this.createBoundChild(exports.DefaultModules.DefaultModule);
      clientSocket.on("error", (error) => {
        internalChildLogger.error(error, "socket error");
      });
      clientSocket.on("disconnect", (reason) => {
        switch (reason) {
          case SocketDisconnectReason.ServerDisconnect:
          case SocketDisconnectReason.V2ServerDisconnect:
            internalChildLogger.info("The socket was forcefully disconnected with socket.disconnect()", {reason});
            break;
          case SocketDisconnectReason.ClientDisconnect:
          case SocketDisconnectReason.V2ClientDisconnect:
            internalChildLogger.info("The client has manually disconnected the socket using socket.disconnect()", {
              reason
            });
            break;
          case SocketDisconnectReason.PingTimeout:
            internalChildLogger.warn("The client did not respond in the pingTimeout range", {reason});
            break;
          case SocketDisconnectReason.TransportClose:
            internalChildLogger.warn("The connection was closed (example: the user has lost connection, or the network was changed from WiFi to 4G)", {reason});
            break;
          case SocketDisconnectReason.TransportError:
            internalChildLogger.warn("The connection has encountered an error", {reason});
            break;
          default:
            internalChildLogger.info("Socket disconnected", {reason});
            break;
        }
      });
    });
  }
  calculateAdditionalFields() {
    return __assign$3(__assign$3({}, this.systemInfo), super.calculateAdditionalFields());
  }
  decompressLogDump(logDump) {
    return logDump.split(ServerSyncTransport.BATCH_SEPARATOR);
  }
  destroy() {
    this.server.close();
    this.server = null;
  }
};
let ServerLogger = ServerLogger2;
ServerLogger.DEFAULT_SERVER_CONFIG = {
  cors: {
    origin: "*:*"
  },
  path: "/ws",
  pingTimeout: 3e3,
  pingInterval: 3e3
};

class DummyLogger {
  error(code, message, data) {
  }
  fatal(code, message, data) {
  }
  warn(message, data) {
  }
  info(message, data) {
  }
  debug(message, data) {
  }
  report(event, data) {
  }
  createBoundChild(module, boundFields) {
    return new DummyLogger();
  }
  getStackTrace() {
    return stacktraceJs.getSync();
  }
  destroy() {
  }
}

(function(SomeEnum2) {
})(exports.SomeEnum || (exports.SomeEnum = {}));

const isNodeEnvironment = new Function("try {return this===global;}catch(e){return false;}")();

exports.BaseLogger = BaseLogger;
exports.ClientLogger = ClientLogger;
exports.DummyLogger = DummyLogger;
exports.ServerLogger = ServerLogger;
//# sourceMappingURL=lib.js.map
