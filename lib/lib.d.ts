import { StackFrame } from 'stacktrace-js';
import { SocketOptions, Socket } from 'socket.io-client';
import { ManagerOptions } from 'socket.io-client/build/manager';
import { ServerOptions, Server } from 'socket.io';

/**
 * 1. Should be stored inside logger
 * 2. Child services (including client/server) should be defined as separate services e.g. VirtualRoomClient
 */
declare enum S2MServices {
    VirtualRoomClient = "VirtualRoomClient",
    VirtualRoomServer = "VirtualRoomServer",
    RecordingCapture = "RecordingCapture",
    RecordingDb = "RecordingDb",
    RecordingProcessing = "RecordingProcessing",
    TestingPlatform = "TestingPlatform"
}

declare enum LogLevel {
    FATAL = 0,
    ERROR = 1,
    WARN = 2,
    INFO = 3,
    DEBUG = 4,
    EVENT = 5
}

declare enum SomeEnum {
}
declare type BasicEnum = typeof SomeEnum;

/**
 * 1. Should be defined inside service
 * 2. May have depth levels defined by boundChild (e.g. Service -> ParentModule -> ChildModule)
 */
declare enum DefaultModules {
    DefaultModule = "App"
}

interface ILogData {
    [key: string]: any;
}
declare type TLogObject<Modules extends BasicEnum, LogDataModel extends ILogData = ILogData> = ILogObject<Modules, LogDataModel> & (IAdditionalLogFields<Modules> | IAdditionalBackendFields<Modules>);
interface ILogObject<Modules extends BasicEnum, LogDataModel extends ILogData = ILogData> {
    data: LogDataModel;
    type: string;
    level: LogLevel;
    message: string;
}
interface IAdditionalLogFields<Modules extends BasicEnum> {
    service: S2MServices;
    module: Modules | DefaultModules.DefaultModule;
    moduleContext: string;
    time: string;
}
interface IAdditionalBackendFields<Modules extends BasicEnum> extends IAdditionalLogFields<Modules> {
    hostname: string;
    pid: string;
}

declare type TBoundFields<LogDataModel, ReportEventModel> = Partial<LogDataModel> & Partial<ReportEventModel>;

interface ILogError<ErrorCode> {
    code: ErrorCode;
    message?: string;
}

interface ILoggerUtils {
    getStackTrace(): StackFrame[];
}
interface ILogger<Modules extends BasicEnum, ErrorCode extends BasicEnum = BasicEnum, LogDataModel extends ILogData = ILogData, ReportEventType extends BasicEnum = BasicEnum, ReportEventModel extends ILogData = ILogData> extends IChildLogger<LogDataModel, ErrorCode, ReportEventType, ReportEventModel>, ILoggerUtils {
    createBoundChild<ChildDataModel extends ILogData = LogDataModel, ChildEventModel extends ILogData = ReportEventModel>(module: Modules, boundFields?: TBoundFields<ChildDataModel, ChildEventModel>): ILogger<Modules, ErrorCode, ChildDataModel, ReportEventType, ChildEventModel>;
}
interface ILoggerInternals<Modules> {
    /**
     * @deprecated: This method is for internal use only. You shouldn't use it outside of logger package
     */
    getParentLogger(): ILoggerInternals<Modules> | null;
    /**
     * @deprecated: This method is for internal use only. You shouldn't use it outside of logger package
     */
    getModule(): Modules;
}
interface IChildLogger<LogDataModel, ErrorCode, ReportEventType, ReportEventModel> {
    fatal(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
    fatal(error: ILogError<ErrorCode>, data?: LogDataModel): void;
    fatal(code: ErrorCode, message?: string, data?: LogDataModel): void;
    fatal(code: ErrorCode, data?: LogDataModel): void;
    error(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
    error(error: ILogError<ErrorCode>, data?: LogDataModel): void;
    error(code: ErrorCode, message?: string, data?: LogDataModel): void;
    error(code: ErrorCode, data?: LogDataModel): void;
    warn(message: string, data?: LogDataModel): void;
    report(event: ReportEventType, data?: ReportEventModel): void;
    info(message: string, data?: LogDataModel): void;
    debug(message: string, data?: LogDataModel): void;
    destroy(): any;
}

interface ILogTransport<ITransportConfig extends object = {}> {
    log<Modules extends BasicEnum, LogDataModel extends ILogData = {}>(message: TLogObject<Modules, LogDataModel>, applyBatch?: boolean, applyDeduplication?: boolean): void;
    setup?(config?: ITransportConfig, logger?: ILogger<any>): void;
    destroy?(): void;
}

interface ILoggerSettings {
    devMode?: boolean;
    exposeStack?: boolean;
    exposeErrorCodeFrame?: boolean;
    parseTemplateMessages?: boolean;
}
/**
 * @TODO:
 * 1. Solve module/service bindings (@see: bunyan child logger)
 * 2. Add support for callstack auto-logging
 * 3. Add support for client/server sync
 * 4. Criteria batch dump solution
 */
declare class BaseLogger<Modules extends BasicEnum, ErrorCode extends BasicEnum = BasicEnum, LogDataModel extends ILogData = ILogData, ReportEventType extends BasicEnum = BasicEnum, ReportEventModel extends ILogData = ILogData> implements ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, LogDataModel, ReportEventType, ReportEventModel>, ILoggerInternals<Modules | DefaultModules.DefaultModule> {
    protected service: S2MServices;
    protected module: Modules | DefaultModules.DefaultModule;
    protected settings: ILoggerSettings;
    protected boundFields?: TBoundFields<LogDataModel, ReportEventModel>;
    protected parent: ILoggerInternals<Modules | DefaultModules.DefaultModule>;
    protected transport: ILogTransport;
    protected moduleContext: Array<Modules | DefaultModules.DefaultModule>;
    constructor(service: S2MServices, module?: Modules | DefaultModules.DefaultModule, settings?: ILoggerSettings, boundFields?: TBoundFields<LogDataModel, ReportEventModel>, parent?: ILoggerInternals<Modules | DefaultModules.DefaultModule>, transport?: ILogTransport);
    protected log(message: string, data?: LogDataModel | ReportEventModel, level?: LogLevel, type?: string): void;
    protected calculateAdditionalFields(): IAdditionalLogFields<Modules> | IAdditionalBackendFields<Modules>;
    private getModuleContext;
    getModule(): Modules | DefaultModules.DefaultModule;
    getParentLogger(): ILoggerInternals<Modules | DefaultModules.DefaultModule> | null;
    error(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
    error(error: ILogError<ErrorCode>, data?: LogDataModel): void;
    error(code: ErrorCode, message?: string, data?: LogDataModel): void;
    error(code: ErrorCode, data?: LogDataModel): void;
    fatal(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
    fatal(error: ILogError<ErrorCode>, data?: LogDataModel): void;
    fatal(code: ErrorCode, message?: string, data?: LogDataModel): void;
    fatal(code: ErrorCode, data?: LogDataModel): void;
    protected logError(logLevel: LogLevel, code: ILogError<ErrorCode> | ErrorCode | LogDataModel, message?: string | LogDataModel, data?: LogDataModel): void;
    warn(message: string, data?: LogDataModel): void;
    info(message: string, data?: LogDataModel): void;
    debug(message: string, data?: LogDataModel): void;
    report(event: ReportEventType, data?: ReportEventModel): void;
    createBoundChild<ChildDataModel extends ILogData = LogDataModel, ChildEventModel extends ILogData = ILogData>(module: Modules | DefaultModules.DefaultModule, boundFields?: TBoundFields<ChildDataModel, ChildEventModel>): ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, ChildDataModel, ReportEventType, ChildEventModel>;
    getStackTrace(): StackFrame[];
    destroy(): void;
}

interface ITransportClientConfig extends Partial<ManagerOptions & SocketOptions> {
    uri?: string;
    clientFactory?: TTransportClientFactory;
}
declare type TTransportClientFactory = (config: ITransportClientConfig) => Socket;

declare class ClientLogger<Module extends BasicEnum, ErrorCode extends BasicEnum = BasicEnum, LogDataModel extends ILogData = ILogData, ReportEventType extends BasicEnum = BasicEnum, ReportEventModel extends ILogData = ILogData> extends BaseLogger<Module, ErrorCode, LogDataModel, ReportEventType, ReportEventModel> {
    protected service: S2MServices;
    protected module: Module | DefaultModules.DefaultModule;
    protected settings: ILoggerSettings;
    protected boundFields: TBoundFields<LogDataModel, ReportEventModel>;
    protected parent: ILoggerInternals<Module | DefaultModules.DefaultModule>;
    constructor(service: S2MServices, module?: Module | DefaultModules.DefaultModule, settings?: ILoggerSettings, boundFields?: TBoundFields<LogDataModel, ReportEventModel>, parent?: ILoggerInternals<Module | DefaultModules.DefaultModule>);
    startToSync(config: ITransportClientConfig): void;
    protected calculateAdditionalFields(): IAdditionalLogFields<Module> | (IAdditionalBackendFields<Module> & {
        timezone: string;
    });
    destroy(): void;
}

declare type TServerFactory = (config: IBackendTransportConfig) => Server;
interface IBackendTransportConfig extends Partial<ServerOptions> {
    serverFactory?: TServerFactory;
    port: string;
}
declare type TSystemInfo = {
    hostname: string;
    pid: string;
};
/**
 * @TODO:
 * 1. should handle auth
 * 2. should batch messages
 * 3. should handle connection issues
 */
declare class ServerLogger<Module extends BasicEnum, ErrorCode extends BasicEnum, LogDataModel extends ILogData = ILogData, ReportEventType extends BasicEnum = BasicEnum, ReportEventModel extends ILogData = ILogData> extends BaseLogger<Module, ErrorCode, LogDataModel, ReportEventType, ReportEventModel> {
    protected service: S2MServices;
    protected module: Module | DefaultModules.DefaultModule;
    protected settings: ILoggerSettings;
    protected boundFields: TBoundFields<LogDataModel, ReportEventModel>;
    protected parent: ILoggerInternals<Module | DefaultModules.DefaultModule>;
    protected server: Server;
    protected systemInfo: TSystemInfo;
    static DEFAULT_SERVER_CONFIG: Partial<IBackendTransportConfig>;
    constructor(service: S2MServices, module?: Module | DefaultModules.DefaultModule, settings?: ILoggerSettings, boundFields?: TBoundFields<LogDataModel, ReportEventModel>, parent?: ILoggerInternals<Module | DefaultModules.DefaultModule>);
    startToSync(serverConfig: IBackendTransportConfig): void;
    protected calculateAdditionalFields(): IAdditionalLogFields<Module> | IAdditionalBackendFields<Module>;
    protected decompressLogDump(logDump: string): string[];
    destroy(): void;
}

declare class DummyLogger<Modules extends BasicEnum, ErrorCode extends BasicEnum = BasicEnum, LogDataModel extends ILogData = ILogData, ReportEventType extends BasicEnum = BasicEnum, ReportEventModel extends ILogData = ILogData> implements ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, LogDataModel, ReportEventType, ReportEventModel> {
    error(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
    error(error: ILogError<ErrorCode>, data?: LogDataModel): void;
    error(code: ErrorCode, message?: string, data?: LogDataModel): void;
    error(code: ErrorCode, data?: LogDataModel): void;
    fatal(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
    fatal(error: ILogError<ErrorCode>, data?: LogDataModel): void;
    fatal(code: ErrorCode, message?: string, data?: LogDataModel): void;
    fatal(code: ErrorCode, data?: LogDataModel): void;
    warn(message: string, data?: LogDataModel): void;
    info(message: string, data?: LogDataModel): void;
    debug(message: string, data?: LogDataModel): void;
    report(event: ReportEventType, data?: ReportEventModel): void;
    createBoundChild<ChildDataModel extends ILogData = LogDataModel, ChildEventModel extends ILogData = ILogData>(module: Modules | DefaultModules.DefaultModule, boundFields?: TBoundFields<ChildDataModel, ChildEventModel>): ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, ChildDataModel, ReportEventType, ChildEventModel>;
    getStackTrace(): StackFrame[];
    destroy(): void;
}

export { BaseLogger, ClientLogger, DefaultModules, DummyLogger, IBackendTransportConfig, ILogData, ILogger, S2MServices, ServerLogger, SomeEnum };
