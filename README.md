# Common backend logger

## Description
A module for logging events or messages, with the ability to add parameters as a JSON object.

## Setup

#### Server
```typescript
import { S2MServices, ServerLogger } from '@s2m/logger';

enum LogErrorCode {
  SERVER = 'SERVER',
  MODULE = 'MODULE',
}

export enum LogModuleServer {
  RoomServer = 'RoomServer',
  Auth = 'Auth',
}

const serverLogger = new ServerLogger<LogModuleServer, LogErrorCode>(S2MServices.VirtualRoomServer);
const roomServerLogger = serverLogger.createBoundChild(LogModuleServer.RoomServer);
const authLogger = serverLogger.createBoundChild(LogModuleServer.Auth);
serverLogger.startToSync({
  serverFactory: () => socket,
});
```
#### Client
```typescript
import { S2MServices, ClientLogger } from '@s2m/logger';

export enum LogModuleClient {
  RoomServer = 'RoomServer',
  Auth = 'Auth',
}

export enum LogModuleClient {
  RecordingCapture = 'Recording',
}

const clientLogger = new ClientLogger<LogModuleClient, LogErrorCode>(S2MServices.VirtualRoomClient);
clientLogger.startToSync({
  clientFactory: () => socket,
});
const recordingCaptureLogger = clientLogger.createBoundChild(LogModuleClient.RecordingCapture);
```
Here we have created logger instances on the server and client, as well as several child instances for different modules.  
In order to sync client instances with the server, we call `startToSync` method on both client and server.  

## Log Method API

### info / warn / debug
```typescript
clientLogger.info(`Create media file ${videoFileName}`); // Log a simple string message

clientLogger.warn(`User update info`, {
  oldInfo: {
    displayName: 'User 1',
    basicColor: 'red',
  },
  newInfo: {
    displayName: 'User 2',
    basicColor: 'green',
  }
}); // Log a string message with an object from second argument and "params" field name
```
These methods accept message and some additional data (optional).

### report
```typescript
enum TestEventType {
  TEST_EXECUTED_ONCE = 'test_executed_once',
  TEST_EXECUTED_TWICE = 'test_executed_twice',
}

serverLogger.report(TestEventType.TEST_EXECUTED_ONCE);
authLogger.report(TestEventType.TEST_EXECUTED_TWICE, {
  callCounter: 44,
});
```
This method accepts `ReportEventType` (will be used as a 'type' log field) and some additional data (optional).  
Output format:
```
Object {
  "data": Object {
    "callCounter": 44,
  },
  "level": 5,
  "message": undefined,
  "module": "Auth",
  "moduleContext": "app.Auth",
  "service": "VirtualRoomServer",
  "time": "2020-01-01T00:00:00.000Z",
  "type": "test_executed_twice",
}
```

### error / fatal

```typescript
class SomeSpecificError extends Error {
  constructor(message: string, public code: TestErrorCode) {
    super(message);
  }
}

enum ErrorCode {
  SOMETHING_WENT_WRONG = 'something_went_wrong',
  SOMETHING_WENT_REALLY_BAD = 'something_went_really_bad',
}

appLogger.error(ErrorCode.SOMETHING_WENT_WRONG, message);
authLogger.error(ErrorCode.SOMETHING_WENT_WRONG);
appLogger.fatal(new SomeSpecificError(message, ErrorCode.SOMETHING_WENT_REALLY_BAD), {
  depthField: {
    counter: 786,
  },
});
authLogger.fatal(ErrorCode.SOMETHING_WENT_REALLY_BAD, message, {
  depthField: {
    counter: 786,
  },
});
```
