import dts from 'rollup-plugin-dts'
import esbuild from 'rollup-plugin-esbuild'
import { nodeResolve } from '@rollup/plugin-node-resolve';
import pkg from "../package.json";

const libName = pkg.main.replace(/\.js$/, '');

const Format = {
 DTS: 'dts',
 CJS: 'cjs',
 MJS: 'mjs',
 ES: 'es',
};

const mainInput = 'src/lib.ts';
const browserInput = 'src/lib.client.ts';

const external = [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {}),
];


const ext = format =>
    format === Format.DTS ? 'd.ts' : format === Format.CJS ? 'js' : Format.MJS;

const bundle = (format, input = mainInput, name = libName) => ({
    input,
    output: {
        file: `${name}.${ext(format)}`,
        format: format === Format.CJS ? Format.CJS : Format.CJS.ES,
        sourcemap: format !== Format.DTS,
    },
    plugins: format === Format.DTS ?
            [dts()] :
            [
                esbuild({
                    tsconfig: 'config/tsconfig.json'
                }),
                // nodeResolve({
                //     browser: true
                // }),
            ]
    ,
    external
    // external: id => !/^[./]/.test(id),
});

export default [
    bundle(Format.ES),
    bundle(Format.CJS),
    bundle(Format.DTS),
    bundle(Format.CJS, browserInput, `${libName}.client`),
    bundle(Format.ES, browserInput, `${libName}.client`),
    // bundle(Format.DTS, browserInput, `${libName}.client`),
]
