// import typescript from '@rollup/plugin-typescript';
import typescript from "rollup-plugin-typescript2";
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';

import pkg from '../package.json'

const external = [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {}),
];

// const isProduction = process.env.NODE_ENV === 'production';
const plugins = [
    // commonjs(),
    // typescript(),
    nodeResolve({browser: true}),
    typescript({
        typescript: require("typescript"),
        tsconfig: "./config/tsconfig.json",
    }),
];

const isProduction = false;

export default [
    {

        input: 'src/lib.client.ts',
        output: {
            file: pkg.browser,
            // dir: 'lib',
            format: "cjs",
            sourcemap: !isProduction
        },
        plugins,
        external
    },
    {

        input: 'src/lib.ts',
        output: {
            file: pkg.main,
            // dir: 'lib',
            format: "cjs",
            sourcemap: !isProduction
        },
        plugins,
        external
    },
];
