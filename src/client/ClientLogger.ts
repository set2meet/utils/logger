import ServerSyncTransport from './transport/ServerSyncTransport';
import BaseLogger, { ILoggerSettings } from '../common/BaseLogger';
import { IAdditionalBackendFields, IAdditionalLogFields, ILogData } from '../common/types/ILogObject';
import { S2MServices } from '../common/types/S2MServices';
import { ILoggerInternals } from '../common/types/ILogger';
import { BasicEnum } from '../common/types/BasicEnum';
import { TBoundFields } from '../common/types/TBoundFields';
import { DefaultModules } from '../common/types/DefaultModules';
import { ITransportClientConfig } from './transport/types/ITransportClientFactory';

export class ClientLogger<
  Module extends BasicEnum,
  ErrorCode extends BasicEnum = BasicEnum,
  LogDataModel extends ILogData = ILogData,
  ReportEventType extends BasicEnum = BasicEnum,
  ReportEventModel extends ILogData = ILogData
> extends BaseLogger<Module, ErrorCode, LogDataModel, ReportEventType, ReportEventModel> {
  constructor(
    protected service: S2MServices,
    protected module: Module | DefaultModules.DefaultModule = DefaultModules.DefaultModule,
    protected settings: ILoggerSettings = {},
    protected boundFields: TBoundFields<LogDataModel, ReportEventModel> = undefined,
    protected parent: ILoggerInternals<Module | DefaultModules.DefaultModule> = undefined
  ) {
    super(service, module, settings, boundFields, parent, new ServerSyncTransport());
  }

  public startToSync(config: ITransportClientConfig): void {
    this.transport.setup(config, this);
  }

  protected calculateAdditionalFields():
    | IAdditionalLogFields<Module>
    | (IAdditionalBackendFields<Module> & { timezone: string }) {
    return {
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      ...super.calculateAdditionalFields(),
    };
  }

  public destroy(): void {
    super.destroy();
    this.transport.destroy();
  }
}
