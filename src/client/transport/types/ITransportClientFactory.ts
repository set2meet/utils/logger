import { Socket, SocketOptions } from 'socket.io-client';
import { ManagerOptions } from 'socket.io-client/build/manager';

export interface ITransportClientConfig extends Partial<ManagerOptions & SocketOptions> {
  uri?: string;
  clientFactory?: TTransportClientFactory;
}

export type TTransportClientFactory = (config: ITransportClientConfig) => Socket;
