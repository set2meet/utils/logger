import { ILogTransport } from '../../common/types/transport/ILogTransport';
import { BasicEnum } from '../../common/types/BasicEnum';
import { IAdditionalBackendFields, IAdditionalLogFields, ILogData, ILogObject } from '../../common/types/ILogObject';

export default class ConsoleTransport implements ILogTransport {
  log<Modules extends BasicEnum, LogDataModel extends ILogData = ILogData>(
    message:
      | (ILogObject<Modules, LogDataModel> & IAdditionalLogFields<Modules>)
      | (ILogObject<Modules, LogDataModel> & IAdditionalBackendFields<Modules>)
  ): void {
    console.log(message);
  }
}
