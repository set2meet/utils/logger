import { io, Socket } from 'socket.io-client';
import { ILogTransport } from '../../common/types/transport/ILogTransport';
import { TransportEvent } from '../../common/types/transport/TransportEvent';
import { IAdditionalLogFields } from '../../common/types/ILogObject';
import { ITransportClientConfig } from './types/ITransportClientFactory';
import ISerializer from '../../common/serializers/ISerializer';
import JSONSerializer from '../../common/serializers/JSONSerializer';
import { BasicEnum } from '../../common/types/BasicEnum';
import { SocketDisconnectReason } from '../../common/types/transport/SocketDisconnectReason';
import { ILogger } from '../../common/types/ILogger';

type TLogStackMap = {
  [key: string]: string[];
};

/**
 * 1. should handle auth
 * 2. should batch messages
 * 3. should handle connection issues (e.g. dump to localStorage)
 */
export default class ServerSyncTransport implements ILogTransport<ITransportClientConfig> {
  public static readonly BATCH_SEPARATOR: string = '_$_';
  public readonly BATCH_DELAY: number = 3000;

  protected logStackMap: TLogStackMap = {};
  protected isInitialized = false;
  protected serializer: ISerializer = new JSONSerializer();
  private client: Socket;

  protected activeBatchOperation: number = null;

  protected dumpLogs(applyDeduplication = true): void {
    this.applyForModuleStack((currentModule, currentStack) => {
      if (currentStack.length) {
        const dump: string = (applyDeduplication
          ? currentStack.filter((value, index, stack) => stack.indexOf(value) === index)
          : currentStack
        ).join(ServerSyncTransport.BATCH_SEPARATOR);

        // handle existing logs
        if (dump) {
          this.sendLogs(dump);
        }

        // cleanup current stack
        currentStack.length = 0;
      }
    });
  }

  protected applyForModuleStack(action: (currentModule: string, currentStack: string[]) => void): void {
    Object.keys(this.logStackMap).forEach((currentModule) =>
      action.call(this, currentModule, this.logStackMap[currentModule])
    );
  }

  protected compress(message: string): string {
    // @TODO: add compression implementation here
    return message;
  }

  protected packLogs(message: Record<string, unknown>): string {
    return this.compress(this.serializer.serialize(message));
  }

  public setup(config?: ITransportClientConfig, logger?: ILogger<any>): void {
    const { clientFactory, uri } = config;

    this.client = clientFactory
      ? clientFactory(config)
      : io(uri, {
          reconnectionDelayMax: 1000,
          timeout: 3000,
          ...config,
        });
    this.client.on('connect', (serverSocket) => {
      this.isInitialized = true;
      this.dumpLogs();
      this.client.on('disconnect', this.onDisconnect);
    });
    this.client.on('connect_timeout', (timeout) => {
      logger.warn('connect_timeout', { timeout });
    });
    this.client.on('connect_error', (error) => {
      logger.error(error, 'socket connect error');
    });
    this.client.on('error', (error) => {
      logger.error(error, 'socket error');
    });
    this.client.on('disconnect', (reason) => {
      logger.info('Socket disconnected', { reason });
    });
  }

  public destroy(): void {
    this.client.disconnect();
    this.logStackMap = {};
  }

  private onDisconnect = () => {
    this.isInitialized = false;
  };

  private getLogStackByModule(module: BasicEnum): string[] {
    const moduleKey: string = module.toString();
    let resultStack = this.logStackMap[moduleKey];

    if (!resultStack) {
      this.logStackMap[moduleKey] = resultStack = [];
    }
    return resultStack;
  }

  protected sendLogs(message: string): void {
    this.client.emit(TransportEvent.LOG, message);
  }

  public log(message: IAdditionalLogFields<BasicEnum>, applyBatch = true, applyDeduplication = true): void {
    const rawMessage: Record<string, unknown> = (message as unknown) as Record<string, unknown>;
    if (applyBatch || !this.isInitialized) {
      this.getLogStackByModule(message.module).push(this.packLogs(rawMessage));

      if (!this.activeBatchOperation && this.isInitialized) {
        // new batch case
        this.activeBatchOperation = window.setTimeout(() => {
          this.dumpLogs(applyDeduplication);
          this.activeBatchOperation = null;
        }, this.BATCH_DELAY);
      }
    } else {
      this.sendLogs(this.packLogs(rawMessage));
    }
  }
}
