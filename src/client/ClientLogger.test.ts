import { ClientLogger } from './ClientLogger';
import { Server, SocketIO } from 'mock-socket';
import { S2MServices } from '../common/types/S2MServices';
import { sleep } from '../common/utils/utils';
import { Socket } from 'socket.io-client';
import { TransportEvent } from '../common/types/transport/TransportEvent';
import { ITransportClientConfig } from './transport/types/ITransportClientFactory';

describe('logger client test', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  const connectionConfig = {
    host: 'http://localhost',
    port: '8321',
    path: '/ws',
  };

  const fakeURL = `${connectionConfig.host}:${connectionConfig.port}`;

  test('should hurr durr', async (done) => {
    const mockServer = new Server(fakeURL);

    jest.spyOn(Date.prototype, 'toISOString').mockReturnValue('2021-01-27T13:14:16.652Z');

    mockServer.on('connection', (socket: any) => {
      socket.on(TransportEvent.LOG, (data) => {
        expect(data).toMatchSnapshot();
        mockServer.close();
        done();
      });
    });

    const clientLogger: ClientLogger<S2MServices> = new ClientLogger<S2MServices>(S2MServices.VirtualRoomClient);
    clientLogger.startToSync({
      uri: fakeURL,
      clientFactory: mockedSocketFactory,
    });
    await sleep(300);
    clientLogger.info('lol message');
  }, 5000);
  test('should batch messages before syncing', async (done) => {
    jest.spyOn(Date.prototype, 'toISOString').mockReturnValue('2021-01-27T13:14:16.652Z');

    const mockServer = new Server(fakeURL);
    let isDump = false;
    mockServer.on('connection', (socket: any) => {
      socket.on(TransportEvent.LOG, (data) => {
        if (!isDump) {
          expect(data).toMatchSnapshot();
          isDump = true;
        }
      });
    });

    const clientLogger: ClientLogger<S2MServices> = new ClientLogger<S2MServices>(S2MServices.VirtualRoomClient);
    clientLogger.info('msg 1');
    clientLogger.info('msg 2');
    clientLogger.startToSync({
      uri: fakeURL,
      clientFactory: mockedSocketFactory,
    });
    await sleep(3000);
    expect(isDump).toBeTruthy();
    const setTimeoutMock = jest.spyOn(window, 'setTimeout');
    clientLogger.info('msg 3');
    clientLogger.info('msg 4');
    clientLogger.info('msg 5');
    expect(setTimeoutMock).toBeCalledTimes(1);
    mockServer.close();
    done();
  });
});

const mockedSocketFactory = (config: ITransportClientConfig) => {
  const socket: Socket = (SocketIO(config.uri) as unknown) as Socket;
  socket.connect = (): Socket => socket;
  socket.id = '1';
  const io = socket.io;
  if (io) {
    io['opts'].query = {};
  } else {
    (socket as any).io = {
      opts: {
        query: {},
      },
    };
  }

  return socket;
};
