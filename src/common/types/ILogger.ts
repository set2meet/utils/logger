import { StackFrame } from 'stacktrace-js';
import { ILogData } from './ILogObject';
import { BasicEnum } from './BasicEnum';
import { TBoundFields } from './TBoundFields';
import { ILogError } from './ILogError';

export interface ILoggerUtils {
  getStackTrace(): StackFrame[];

  /**
   * @TODO: we should use some compression algorithms for deduplication in order to decrease package size while sending
   * logs over network (between client/server or shipper/logstash)
   */
  //
  //
  // compressLog(rawLog);
  // uncompressLog(compressedLog);
}

export interface ILogger<
  Modules extends BasicEnum,
  ErrorCode extends BasicEnum = BasicEnum,
  LogDataModel extends ILogData = ILogData,
  ReportEventType extends BasicEnum = BasicEnum,
  ReportEventModel extends ILogData = ILogData
> extends IChildLogger<LogDataModel, ErrorCode, ReportEventType, ReportEventModel>,
    ILoggerUtils {
  // @TODO: allow to bind any field
  createBoundChild<ChildDataModel extends ILogData = LogDataModel, ChildEventModel extends ILogData = ReportEventModel>(
    module: Modules,
    // @TODO: validation here currently doesn't work, but seems it is lang issue so we just waiting for a fix
    boundFields?: TBoundFields<ChildDataModel, ChildEventModel>
  ): ILogger<Modules, ErrorCode, ChildDataModel, ReportEventType, ChildEventModel>;
}

export interface ILoggerInternals<Modules> {
  /**
   * @deprecated: This method is for internal use only. You shouldn't use it outside of logger package
   */
  getParentLogger(): ILoggerInternals<Modules> | null;
  /**
   * @deprecated: This method is for internal use only. You shouldn't use it outside of logger package
   */
  getModule(): Modules;
}

export interface IChildLogger<LogDataModel, ErrorCode, ReportEventType, ReportEventModel> {
  fatal(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
  fatal(error: ILogError<ErrorCode>, data?: LogDataModel): void;
  fatal(code: ErrorCode, message?: string, data?: LogDataModel): void;
  fatal(code: ErrorCode, data?: LogDataModel): void;

  error(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
  error(error: ILogError<ErrorCode>, data?: LogDataModel): void;
  error(code: ErrorCode, message?: string, data?: LogDataModel): void;
  error(code: ErrorCode, data?: LogDataModel): void;

  warn(message: string, data?: LogDataModel): void;

  report(event: ReportEventType, data?: ReportEventModel): void;
  info(message: string, data?: LogDataModel): void;
  debug(message: string, data?: LogDataModel): void;
  destroy();
}
