export enum LogLevel {
  // The service/app is going to stop or become unusable now. An operator should definitely look into this soon.
  FATAL,
  // Fatal for a particular request, but the service/app continues servicing other requests. An operator should look at this soon(ish).
  ERROR,
  // A note on something that should probably be looked at by an operator eventually.
  WARN,
  // Detail on regular operation.
  INFO,
  // Anything else, i.e. too verbose to be included in "info" level.
  DEBUG,
  // Logging from external libraries used by your app or very detailed application logging.
  EVENT,
}
