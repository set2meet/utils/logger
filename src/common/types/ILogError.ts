export interface ILogError<ErrorCode> {
  code: ErrorCode;
  message?: string;
}

export default class ErrorWithCode<ErrorCode> extends Error implements ILogError<ErrorCode> {
  constructor(public code: ErrorCode) {
    super(code.toString());
  }
}

// /**
//  * Object representing an error with a stack trace
//  * Partially copied from https://github.com/fullstack-build/tslog/blob/03cea194225d807a3528a0f183ef486c33ac73ab/src/LoggerWithoutCallSite.ts
//  * @public
//  */
// export interface IErrorObject {
//     /** Is this object an error? */
//     isError: true;
//     /** Name of the error*/
//     name: string;
//     /** Error message */
//     message: string;
//     /** additional Error details */
//     details: object;
//     /** native Error object */
//     nativeError: Error;
//     /** Stack trace of the error */
//     stack: IStackFrame[];
//     /** Code frame of the error */
//     codeFrame?: ICodeFrame;
// }
//
// /**
//  * Code frame of an error
//  * Partially copied from https://github.com/fullstack-build/tslog/blob/03cea194225d807a3528a0f183ef486c33ac73ab/src/LoggerWithoutCallSite.ts
//  * @public
//  * */
// export interface ICodeFrame {
//     firstLineNumber: number;
//     lineNumber: number;
//     columnNumber: number | undefined;
//     linesBefore: string[];
//     relevantLine: string;
//     linesAfter: string[];
// }
//
// /**
//  * All relevant information about a log message
//  * Partially copied from https://github.com/fullstack-build/tslog/blob/03cea194225d807a3528a0f183ef486c33ac73ab/src/LoggerWithoutCallSite.ts
//  * @public
//  */
// export interface IStackFrame {
//     /** Relative path based on the main folder */
//     filePath: string | undefined;
//     /** Full path */
//     fullFilePath: string | undefined;
//     /** Name of the file */
//     fileName: string | undefined;
//     /** Line number */
//     lineNumber: number | undefined;
//     /** Column Name */
//     columnNumber: number | undefined;
//     /** Called from constructor */
//     isConstructor: boolean | undefined;
//     /** Name of the function */
//     functionName: string | undefined;
//     /** Name of the class */
//     typeName: string | undefined;
//     /** Name of the Method */
//     methodName: string | undefined;
// }
