export type TBoundFields<LogDataModel, ReportEventModel> = Partial<LogDataModel> & Partial<ReportEventModel>;
