/**
 * 1. Should be stored inside logger
 * 2. Child services (including client/server) should be defined as separate services e.g. VirtualRoomClient
 */
export enum S2MServices {
  VirtualRoomClient = 'VirtualRoomClient',
  VirtualRoomServer = 'VirtualRoomServer',
  RecordingCapture = 'RecordingCapture',
  RecordingDb = 'RecordingDb',
  RecordingProcessing = 'RecordingProcessing',
  TestingPlatform = 'TestingPlatform',
}
