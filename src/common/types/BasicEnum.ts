// export type BasicEnum = Record<string | number, string> | Record<string, number>;
// { [key: number]: string | number };

// export interface BasicEnum {[key: number]: string | number}

// export type BasicEnum = {[key: number]: string | number};
// export type BasicEnum<E> = Record<keyof E, number | string> & { [k: number]: string };

// type BasicEnum<E> = Record<keyof E, number | string> & { [k: number]: keyof E }

// function lol<T extends BasicEnum<T>>(val:T){
//     console.log(val);
// }

export enum SomeEnum {}

export type BasicEnum = typeof SomeEnum;
// export type BasicEnum<E = Record<string | number, unknown>> = Record<keyof E, number | string> & { [k: number]: string };
// export type BasicEnum<E> = Record<keyof E, number | string> & { [k: number]: keyof E }
// function acceptEnum<E extends BasicEnum<E>>(
//     myEnum: E
// ): void {
//     // do something with myEnum... what's your use case anyway?
// }

// function lol<T extends BasicEnum>(val:T){
//     console.log(val);
// }
//
// enum LolEnum {
//     X= 'lol',
//     Y = '14',
//     Z = 'ge'
// }
//
// enum LolEnum1 {
//     X,
//     Y,
//     Z
// }
// lol<LolEnum1>(LolEnum1.X);
