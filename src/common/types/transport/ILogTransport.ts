import { ILogData, TLogObject } from '../ILogObject';
import { BasicEnum } from '../BasicEnum';
import { ILogger } from '../ILogger';

export interface ILogTransport<ITransportConfig extends object = {}> {
  log<Modules extends BasicEnum, LogDataModel extends ILogData = {}>(
    message: TLogObject<Modules, LogDataModel>,
    applyBatch?: boolean,
    applyDeduplication?: boolean
  ): void;
  setup?(config?: ITransportConfig, logger?: ILogger<any>): void;
  destroy?(): void;
}
