export enum SocketDisconnectReason {
  ServerDisconnect = 'io server disconnect',
  V2ServerDisconnect = 'server namespace disconnect',
  ClientDisconnect = 'io client disconnect',
  V2ClientDisconnect = 'client namespace disconnect',
  PingTimeout = 'ping timeout',
  TransportClose = 'transport close',
  TransportError = 'transport error',
}
