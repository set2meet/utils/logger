/**
 * 1. Should be defined inside service
 * 2. May have depth levels defined by boundChild (e.g. Service -> ParentModule -> ChildModule)
 */
export enum DefaultModules {
  DefaultModule = 'App',
}
