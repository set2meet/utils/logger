import { LogLevel } from './LogLevel';
import { S2MServices } from './S2MServices';
import { BasicEnum } from './BasicEnum';
import { DefaultModules } from './DefaultModules';

export interface ILogData {
  // @TODO: should enforce serializable typedef
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}

export type TLogObject<Modules extends BasicEnum, LogDataModel extends ILogData = ILogData> = ILogObject<
  Modules,
  LogDataModel
> &
  (IAdditionalLogFields<Modules> | IAdditionalBackendFields<Modules>);

export interface ILogObject<Modules extends BasicEnum, LogDataModel extends ILogData = ILogData> {
  data: LogDataModel;
  type: string;
  level: LogLevel;
  message: string;
}

export interface IAdditionalLogFields<Modules extends BasicEnum> {
  service: S2MServices;
  module: Modules | DefaultModules.DefaultModule;
  moduleContext: string;
  time: string;
}

export interface IAdditionalBackendFields<Modules extends BasicEnum> extends IAdditionalLogFields<Modules> {
  hostname: string;
  pid: string;
}
