// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const sleep = (duration = 0): Promise<any> => new Promise((resolve) => setTimeout(resolve, duration));
export const isRecordEmpty = (record: Record<string, unknown>): boolean => {
  for (const i in record) return false;
  return true;
};
