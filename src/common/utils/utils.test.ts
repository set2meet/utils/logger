import { isRecordEmpty } from './utils';

describe('utils test', () => {
  test('should correctly check for empty object ', () => {
    expect(isRecordEmpty({})).toBeTruthy();
    expect(isRecordEmpty({ someKey: 'someVal' })).toBeFalsy();
  });
});
