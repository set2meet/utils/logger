import { getSync as getStackTrace, StackFrame } from 'stacktrace-js';
import { ILogger, ILoggerInternals } from './types/ILogger';
import { ILogData } from './types/ILogObject';
import { BasicEnum } from './types/BasicEnum';
import { DefaultModules } from './types/DefaultModules';
import { ILogError } from './types/ILogError';
import { TBoundFields } from './types/TBoundFields';

export default class DummyLogger<
  Modules extends BasicEnum,
  ErrorCode extends BasicEnum = BasicEnum,
  LogDataModel extends ILogData = ILogData,
  ReportEventType extends BasicEnum = BasicEnum,
  ReportEventModel extends ILogData = ILogData
> implements
    ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, LogDataModel, ReportEventType, ReportEventModel> {
  public error(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
  public error(error: ILogError<ErrorCode>, data?: LogDataModel): void;
  public error(code: ErrorCode, message?: string, data?: LogDataModel): void;
  public error(code: ErrorCode, data?: LogDataModel): void;
  public error(
    code: ILogError<ErrorCode> | ErrorCode | LogDataModel,
    message?: string | LogDataModel,
    data?: LogDataModel
  ): void {
    // Do nithing
  }

  public fatal(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
  public fatal(error: ILogError<ErrorCode>, data?: LogDataModel): void;
  public fatal(code: ErrorCode, message?: string, data?: LogDataModel): void;
  public fatal(code: ErrorCode, data?: LogDataModel): void;
  public fatal(
    code: ILogError<ErrorCode> | ErrorCode | LogDataModel,
    message?: string | LogDataModel,
    data?: LogDataModel
  ): void {
    // Do nithing
  }

  public warn(message: string, data?: LogDataModel): void {
    // Do nithing
  }

  public info(message: string, data?: LogDataModel): void {
    // Do nithing
  }

  public debug(message: string, data?: LogDataModel): void {
    // Do nithing
  }

  public report(event: ReportEventType, data?: ReportEventModel): void {
    // Do nithing
  }

  public createBoundChild<ChildDataModel extends ILogData = LogDataModel, ChildEventModel extends ILogData = ILogData>(
    module: Modules | DefaultModules.DefaultModule,
    boundFields?: TBoundFields<ChildDataModel, ChildEventModel>
  ): ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, ChildDataModel, ReportEventType, ChildEventModel> {
    return new DummyLogger<Modules, ErrorCode>();
  }

  public getStackTrace(): StackFrame[] {
    return getStackTrace();
  }

  public destroy(): void {
    // Do nithing
  }
}
