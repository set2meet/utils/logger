import { getSync as getStackTrace, StackFrame } from 'stacktrace-js';
import { compile as createTemplateString } from 'handlebars';
import { ILogTransport } from './types/transport/ILogTransport';
import { LogLevel } from './types/LogLevel';
import { S2MServices } from './types/S2MServices';
import { ILogger, ILoggerInternals } from './types/ILogger';
import { IAdditionalBackendFields, IAdditionalLogFields, ILogData } from './types/ILogObject';
import { BasicEnum } from './types/BasicEnum';
import { DefaultModules } from './types/DefaultModules';
import { ILogError } from './types/ILogError';
import ConsoleTransport from '../client/transport/ConsoleTransport';
import { TBoundFields } from './types/TBoundFields';
import { isRecordEmpty } from './utils/utils';

const CALLSTACK_LOG_TYPE = 'callstack_log_type';

export interface ILoggerSettings {
  devMode?: boolean;
  exposeStack?: boolean;
  exposeErrorCodeFrame?: boolean;
  parseTemplateMessages?: boolean;
}

/**
 * @TODO:
 * 1. Solve module/service bindings (@see: bunyan child logger)
 * 2. Add support for callstack auto-logging
 * 3. Add support for client/server sync
 * 4. Criteria batch dump solution
 */
export default class BaseLogger<
  Modules extends BasicEnum,
  ErrorCode extends BasicEnum = BasicEnum,
  LogDataModel extends ILogData = ILogData,
  ReportEventType extends BasicEnum = BasicEnum,
  ReportEventModel extends ILogData = ILogData
> implements
    ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, LogDataModel, ReportEventType, ReportEventModel>,
    ILoggerInternals<Modules | DefaultModules.DefaultModule> {
  protected moduleContext: Array<Modules | DefaultModules.DefaultModule> = this.getModuleContext();

  constructor(
    protected service: S2MServices,
    protected module: Modules | DefaultModules.DefaultModule = DefaultModules.DefaultModule,
    protected settings: ILoggerSettings = {},
    protected boundFields?: TBoundFields<LogDataModel, ReportEventModel>,
    protected parent: ILoggerInternals<Modules | DefaultModules.DefaultModule> = null,
    protected transport: ILogTransport = new ConsoleTransport()
  ) {}

  protected log(
    message: string,
    data?: LogDataModel | ReportEventModel,
    level: LogLevel = LogLevel.INFO,
    type = CALLSTACK_LOG_TYPE
  ): void {
    const { devMode, parseTemplateMessages } = this.settings;

    if (devMode && type === CALLSTACK_LOG_TYPE) {
      const stackTrace: StackFrame[] = this.getStackTrace();

      type = stackTrace.find((value) => value.functionName.indexOf(this.constructor.name) !== 0).functionName;
    }

    const logMeta = {
      data:
        this.boundFields || data
          ? {
              ...this.boundFields,
              ...data,
            }
          : undefined,
      type,
      level,
      ...this.calculateAdditionalFields(),
    };

    if (parseTemplateMessages && message) {
      message = createTemplateString(message)(logMeta);
    }

    this.transport.log({ ...logMeta, message });
  }

  // protected sanitizeUserData(data:LogDataModel | ReportEventModel):LogDataModel | ReportEventModel{
  //   Object.keys(data).forEach(key => {
  //     const currentValue = data[key];
  //     const errorObject: IErrorObject = currentValue as IErrorObject;
  //     if (typeof currentValue === "object" && errorObject.isError) {
  //       return {
  //         ...errorObject,
  //         nativeError: undefined,
  //         errorString: this.formatAndHideSensitive(
  //             errorObject.nativeError
  //         ),
  //       } as IErrorObjectStringifiable;
  //     } else if (typeof currentValue === "object") {
  //       return this._inspectAndHideSensitive(
  //           currentValue,
  //           this.settings.jsonInspectOptions
  //       );
  //     } else {
  //       return this.formatAndHideSensitive(currentValue);
  //     }
  //   });
  //   return data;
  // }
  //
  // protected formatAndHideSensitive(){
  //
  // }

  protected calculateAdditionalFields(): IAdditionalLogFields<Modules> | IAdditionalBackendFields<Modules> {
    return {
      service: this.service,
      module: this.module || DefaultModules.DefaultModule,
      moduleContext: this.moduleContext.join('.'),
      time: new Date().toISOString(),
    };
  }

  private getModuleContext(): Array<Modules | DefaultModules.DefaultModule> {
    const result = [this.getModule()];
    let currentNode: ILoggerInternals<Modules | DefaultModules.DefaultModule> = this.getParentLogger();

    while (currentNode) {
      result.push(currentNode.getModule());
      currentNode = currentNode.getParentLogger();
    }

    return result.reverse();
  }

  public getModule(): Modules | DefaultModules.DefaultModule {
    return this.module;
  }

  public getParentLogger(): ILoggerInternals<Modules | DefaultModules.DefaultModule> | null {
    return this.parent;
  }

  public error(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
  public error(error: ILogError<ErrorCode>, data?: LogDataModel): void;
  public error(code: ErrorCode, message?: string, data?: LogDataModel): void;
  public error(code: ErrorCode, data?: LogDataModel): void;
  public error(
    code: ILogError<ErrorCode> | ErrorCode | LogDataModel,
    message?: string | LogDataModel,
    data?: LogDataModel
  ): void {
    this.logError(LogLevel.ERROR, code, message, data);
  }

  public fatal(error: ILogError<ErrorCode>, message?: string, data?: LogDataModel): void;
  public fatal(error: ILogError<ErrorCode>, data?: LogDataModel): void;
  public fatal(code: ErrorCode, message?: string, data?: LogDataModel): void;
  public fatal(code: ErrorCode, data?: LogDataModel): void;
  public fatal(
    code: ILogError<ErrorCode> | ErrorCode | LogDataModel,
    message?: string | LogDataModel,
    data?: LogDataModel
  ): void {
    this.logError(LogLevel.FATAL, code, message, data);
  }

  protected logError(
    logLevel: LogLevel,
    code: ILogError<ErrorCode> | ErrorCode | LogDataModel,
    message?: string | LogDataModel,
    data?: LogDataModel
  ): void {
    let logData: Record<string, unknown> = data || {};
    let logMessage: string;

    if (typeof message !== 'string') {
      logData = { ...logData, ...message };
    } else {
      logMessage = message;
    }

    if (typeof code === 'string' || typeof code === 'number') {
      logData.code = code;
      if (!logMessage) logMessage = logData.code.toString();
    } else {
      const error = code as ILogError<ErrorCode>;
      logData.error = code;
      logData.code = error.code;
      if (!logMessage) logMessage = error.message || error.code.toString();
    }

    this.log(logMessage, isRecordEmpty(logData) ? undefined : (logData as LogDataModel), logLevel);
  }

  public warn(message: string, data?: LogDataModel): void {
    this.log(message, data, LogLevel.WARN);
  }

  public info(message: string, data?: LogDataModel): void {
    this.log(message, data, LogLevel.INFO);
  }

  public debug(message: string, data?: LogDataModel): void {
    this.log(message, data, LogLevel.DEBUG);
  }

  public report(event: ReportEventType, data?: ReportEventModel): void {
    this.log(undefined, data, LogLevel.EVENT, event.toString());
  }

  /**
   * Created child logger for module.
   *
   * @param module module name (used in 'moduleContext' and 'module' fields).
   * For instance we will receive {"moduleContext": "app.auth", "module": "auth"} for the 'auth' child logger.
   * @param boundFields additional meta-data
   */
  public createBoundChild<ChildDataModel extends ILogData = LogDataModel, ChildEventModel extends ILogData = ILogData>(
    module: Modules | DefaultModules.DefaultModule,
    boundFields?: TBoundFields<ChildDataModel, ChildEventModel>
  ): ILogger<Modules | DefaultModules.DefaultModule, ErrorCode, ChildDataModel, ReportEventType, ChildEventModel> {
    return new BaseLogger<Modules>(
      this.service,
      module,
      this.settings,
      this.boundFields || boundFields ? { ...this.boundFields, ...boundFields } : undefined,
      this,
      this.transport
    );
  }

  public getStackTrace(): StackFrame[] {
    return getStackTrace();
  }

  public destroy(): void {
    // @TODO: should also destroy each child
  }
}
