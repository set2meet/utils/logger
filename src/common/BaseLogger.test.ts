import BaseLogger from './BaseLogger';
import { ServerLogger } from '../server/ServerLogger';
import { ClientLogger } from '../client/ClientLogger';
import { sleep } from './utils/utils';
import { S2MServices } from './types/S2MServices';
import { TLogObject } from './types/ILogObject';

enum TestModule {
  App = 'app',
  Auth = 'auth',
  LoginPage = 'loginPage',
  Presentation = 'presentation',
  PresentationViewer = 'presentationViewer',
  PresentationEditor = 'presentationEditor',
  EncryptionManager = 'encryptionManager',
  ResourceManager = 'resourceManager',
}

enum TestEventType {
  TEST_EXECUTED_ONCE = 'test_executed_once',
  TEST_EXECUTED_TWICE = 'test_executed_twice',
}

enum TestErrorCode {
  SOMETHING_WENT_WRONG = 'something_went_wrong',
  SOMETHING_WENT_REALLY_BAD = 'something_went_really_bad',
}

interface ITestEventModel {
  callCounter: number;
}

interface ITestLogDataModel {
  depthField: {
    counter: number;
  };
  optionalDepthField?: {
    optionalFieldOne?: number;
    optionalFieldTwo?: number;
  };
}

class TestError extends Error {
  constructor(message: string, public code: TestErrorCode) {
    super(message);
  }
}

/**
 * @TODO: generic typedefs seems doesn't work for now, need to fix that
 */
describe('logger debug', () => {
  const TestEntityInstance = {
    firstLoggerContextMethod: null,
    secondLoggerContextMethod: null,
    thirdLoggerContextMethod: null,
  };

  test(
    'should support basic log methods',
    (TestEntityInstance.firstLoggerContextMethod = () => {
      let consoleCache: Record<string, unknown>;

      jest.spyOn(global.console, 'log').mockImplementation((logObject) => {
        logObject.time = new Date('2020-01-01').toISOString();
        consoleCache = logObject;
      });
      const appLogger = new BaseLogger<
        TestModule,
        TestErrorCode,
        ITestLogDataModel | Error,
        TestEventType,
        ITestEventModel
      >(S2MServices.VirtualRoomClient, TestModule.App, {
        devMode: true,
        parseTemplateMessages: true,
      });
      const authLogger = appLogger.createBoundChild(TestModule.Auth);
      const loginPageLogger = authLogger.createBoundChild(TestModule.LoginPage);

      const message = 'something went wrong';

      appLogger.warn(message);
      expect(consoleCache).toMatchSnapshot();

      authLogger.warn(message);
      expect(consoleCache).toMatchSnapshot();

      appLogger.error(TestErrorCode.SOMETHING_WENT_WRONG, message);
      expect(consoleCache).toMatchSnapshot();

      authLogger.error(TestErrorCode.SOMETHING_WENT_WRONG);
      expect(consoleCache).toMatchSnapshot();

      appLogger.fatal(new TestError(message, TestErrorCode.SOMETHING_WENT_REALLY_BAD), {
        depthField: {
          counter: 786,
        },
      });

      authLogger.fatal(TestErrorCode.SOMETHING_WENT_REALLY_BAD, message, {
        depthField: {
          counter: 786,
        },
      });
      expect(consoleCache).toMatchSnapshot();

      authLogger.fatal(new TestError(message, TestErrorCode.SOMETHING_WENT_REALLY_BAD));
      expect(consoleCache).toMatchSnapshot();

      TestEntityInstance.secondLoggerContextMethod = () => loginPageLogger.warn(message);
      TestEntityInstance.secondLoggerContextMethod();
      expect(consoleCache).toMatchSnapshot();

      loginPageLogger.report(TestEventType.TEST_EXECUTED_ONCE);
      expect(consoleCache).toMatchSnapshot();

      loginPageLogger.report(TestEventType.TEST_EXECUTED_TWICE, {
        callCounter: 44,
      });
      expect(consoleCache).toMatchSnapshot();

      // @TODO: think do we really need this cause it should affect performance in some way
      loginPageLogger.info('current depthCounter value in {{moduleContext}} is {{data.depthField.counter}}', {
        depthField: {
          counter: 44,
        },
      });

      expect(consoleCache).toMatchSnapshot();
    })
  );

  test(
    'should correctly bind fixed fields for child logger',
    (TestEntityInstance.thirdLoggerContextMethod = () => {
      let consoleCache: Record<string, unknown>;

      jest.spyOn(global.console, 'log').mockImplementation((logObject) => {
        logObject.time = new Date('2020-01-01').toISOString();
        consoleCache = logObject;
      });

      const initialBoundFields: ITestLogDataModel = {
        depthField: {
          counter: 0,
        },
      };

      const appLogger = new BaseLogger<
        TestModule,
        TestErrorCode,
        ITestLogDataModel | Error,
        TestEventType,
        ITestEventModel
      >(
        S2MServices.VirtualRoomClient,
        TestModule.App,
        {
          devMode: true,
          parseTemplateMessages: true,
        },
        initialBoundFields
      );

      appLogger.info('bound fields is applied to the top level logger');
      expect(consoleCache.data).toMatchObject(initialBoundFields);

      const presentationLogger = appLogger.createBoundChild(TestModule.Presentation);
      presentationLogger.info("child doesn't loose bound field");
      expect(consoleCache.data).toMatchObject(initialBoundFields);

      const viewerLogger = presentationLogger.createBoundChild(TestModule.PresentationViewer);
      viewerLogger.info("next child doesn't loose it too");
      expect(consoleCache.data).toMatchObject(initialBoundFields);

      const firstFieldOverride: ITestLogDataModel = {
        depthField: {
          counter: 44,
        },
      };
      const editorLogger = presentationLogger.createBoundChild(TestModule.PresentationEditor, firstFieldOverride);
      editorLogger.info('we can override bound fields for the first child with unbound fields');
      expect(consoleCache.data).toMatchObject(firstFieldOverride);

      const loginLogger = editorLogger.createBoundChild(TestModule.LoginPage);
      loginLogger.info('then they are inherited correctly by next child');
      expect(consoleCache.data).toMatchObject(firstFieldOverride);

      const secondFieldOverride: ITestLogDataModel = {
        depthField: {
          counter: 85,
        },
        optionalDepthField: {
          optionalFieldOne: 57,
        },
      };
      const authLogger = editorLogger.createBoundChild(TestModule.Auth, secondFieldOverride);
      authLogger.info('and can be overridden as well');
      expect(consoleCache.data).toMatchObject(secondFieldOverride);

      const boundFieldsToMerge: ITestLogDataModel = {
        depthField: {
          counter: 16,
        },
        optionalDepthField: {
          optionalFieldTwo: 78,
        },
      };

      const resourceLogger = editorLogger.createBoundChild(TestModule.ResourceManager, boundFieldsToMerge);
      resourceLogger.info('child fields can be merged');
      expect(consoleCache.data).toMatchObject({
        ...secondFieldOverride,
        ...boundFieldsToMerge,
      });
    })
  );

  test(
    'should correctly sync client/backend logs',
    (TestEntityInstance.firstLoggerContextMethod = async (done) => {
      const consoleCache: string[] = [];

      const serverLogger: ServerLogger<TestModule, TestErrorCode> = new ServerLogger(S2MServices.VirtualRoomServer);
      const clientLogger: ClientLogger<TestModule, TestErrorCode> = new ClientLogger(S2MServices.VirtualRoomClient);

      const connectionConfig = {
        host: 'http://localhost',
        port: '8321',
        path: '/ws',
      };

      jest.spyOn(global.console, 'log').mockImplementation((logObject) => {
        consoleCache.push(logObject);
      });
      jest.spyOn(Date.prototype, 'toISOString').mockReturnValue('2020-01-01T00:00:00.000Z');

      /**
       * @TODO: move this options into config & ensure https!
       */
      serverLogger.startToSync({
        port: connectionConfig.port,
        path: connectionConfig.path,
      });
      clientLogger.startToSync({
        uri: connectionConfig.host + ':' + connectionConfig.port,
        port: connectionConfig.port,
        path: connectionConfig.path,
      });

      clientLogger.info('lol pre test');

      // tslint:disable-next-line:no-magic-numbers
      await sleep(100);

      clientLogger.info('lol past test 0');
      clientLogger.info('lol past test 1');
      clientLogger.info('lol past test 2');
      clientLogger.info('lol past test 2');
      clientLogger.info('lol past test 0');
      clientLogger.info('lol past test 3');

      // tslint:disable-next-line:no-magic-numbers
      await sleep(4000);

      expect(consoleCache).toMatchSnapshot();

      clientLogger.destroy();
      serverLogger.destroy();

      done();
    })
  );
  test('should correctly handle logs order', async (done) => {
    const consoleCache: TLogObject<any>[] = [];

    const serverLogger: ServerLogger<TestModule, TestErrorCode> = new ServerLogger(S2MServices.VirtualRoomServer);
    const clientLogger: ClientLogger<TestModule, TestErrorCode> = new ClientLogger(S2MServices.VirtualRoomClient);

    const connectionConfig = {
      host: 'http://localhost',
      port: '8321',
      path: '/ws',
    };

    jest.spyOn(global.console, 'log').mockImplementation((logObject) => {
      logObject.time = new Date('2020-01-01').toISOString();
      consoleCache.push(logObject);
    });

    /**
     * @TODO: move this options into config & ensure https!
     */
    serverLogger.startToSync({
      port: connectionConfig.port,
      path: connectionConfig.path,
    });
    clientLogger.startToSync({
      uri: connectionConfig.host + ':' + connectionConfig.port,
      port: connectionConfig.port,
      path: connectionConfig.path,
    });
    // tslint:disable-next-line:no-magic-numbers
    await sleep(100);

    clientLogger.info('lol past test 0');
    clientLogger.info('lol past test 1');
    clientLogger.info('lol past test 2');
    clientLogger.info('lol past test 3');

    // tslint:disable-next-line:no-magic-numbers
    await sleep(4000);

    expect(consoleCache[consoleCache.length - 1].message).toEqual('lol past test 3');

    clientLogger.destroy();
    serverLogger.destroy();

    done();
  });
});
