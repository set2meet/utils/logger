import ISerializer from './ISerializer';
import safeJsonStringify from 'safe-json-stringify';

/**
 * We use the custom one because of possible cyclical structures which need to be handled
 */
export default class JSONSerializer implements ISerializer {
  serialize(data: Record<string, unknown>): string {
    return this.serializeToJSON(data);
  }

  deserialize(serializedData: string): Record<string, unknown> {
    return JSON.parse(serializedData);
  }

  /**
   * Based on
   * https://github.com/trentm/node-bunyan/blob/a72af248b57a908a5d39e72b7e9efed7b24e5808/lib/bunyan.js#L1210
   */
  protected serializeToJSON(data: Record<string, unknown>): string {
    try {
      // try to use vanilla method for best performance
      return JSON.stringify(data);
    } catch (firstException) {
      try {
        // try to handle cyclical structures in fast manner
        const seen: Set<unknown> = new Set<unknown>();
        return JSON.stringify(data, (key: string, value: unknown) => {
          if (!value || typeof value !== 'object') return value;
          if (seen.has(value)) return '[Circular]';
          seen.add(value);
          return value;
        });
      } catch (secondException) {
        // use safeJsonStringify in case we can't
        return safeJsonStringify(data);
      }
    }
  }
}
