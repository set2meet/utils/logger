export default interface ISerializer {
  serialize(data: Record<string, unknown>): string;
  deserialize(serializedData: string): Record<string, unknown>;
}
