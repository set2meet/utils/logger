/**
 * Based on
 * https://github.com/trentm/node-bunyan/blob/a72af248b57a908a5d39e72b7e9efed7b24e5808/lib/bunyan.js#L1127
 */

type TSerializedError = {
  message: string;
  name: string;
  stack?: string;
  code?: string;
  signal?: string;
};

type TExtendedError = {
  code?: string;
  signal?: string;
  cause?: () => Error;
};

const serializeError = (error: Error & TExtendedError): TSerializedError => {
  if (!error || !error.stack) {
    return error;
  }

  return {
    message: error.message,
    name: error.name,
    stack: getFullErrorStack(error),
    signal: error.signal,
    code: error.code,
  };
};

/*
 * This function dumps long stack traces for exceptions having a cause()
 * method. The error classes from
 * [verror](https://github.com/davepacheco/node-verror) and
 * [restify v2.0](https://github.com/mcavage/node-restify) are examples.
 *
 * Based on `dumpException` in
 * https://github.com/davepacheco/node-extsprintf/blob/master/lib/extsprintf.js
 */
const getFullErrorStack = (error: Error & TExtendedError): string => {
  let result = error.stack || error.toString();
  if (error.cause && typeof error.cause === 'function') {
    const cex = error.cause();
    if (cex) {
      result += '\nCaused by: ' + getFullErrorStack(cex);
    }
  }
  return result;
};

export default serializeError;
