import { S2MServices } from './common/types/S2MServices';
import { ILogger } from './common/types/ILogger';
import { ClientLogger } from './client/ClientLogger';
import { ServerLogger, IBackendTransportConfig } from './server/ServerLogger';
import BaseLogger from './common/BaseLogger';
// import { type } from "os";
import DummyLogger from './common/DummyLogger';
import { ILogData } from './common/types/ILogObject';
import { SomeEnum } from './common/types/BasicEnum';
import { DefaultModules } from './common/types/DefaultModules';

const isNodeEnvironment = new Function('try {return this===global;}catch(e){return false;}')();

// // export fake BackendLogger for client
// let BackendLogger = class{
//     constructor() {
//         throw new Error(
//             `You are trying to use backend logger inside client environment!
//             Consider importing ClientLogger instead.`
//         )
//     }
// };
//
// if(!isNodeEnvironment){
//     console.log(isNodeEnvironment);
//     BackendLogger = require('./server/BackendLogger')
// }
export { ServerLogger, ClientLogger, S2MServices, BaseLogger, DummyLogger, SomeEnum, DefaultModules };
export type { ILogger, ILogData, IBackendTransportConfig };
//
// const getClientLogger = ():ILogger => {
//     return ClientLogger;
// };
//
// const getBackendLogger = ():Promise<ILogger> => {
//     return import('./server/BackendLogger');
// };
