import { S2MServices } from './common/types/S2MServices';
import { ClientLogger } from './client/ClientLogger';

// export fake ServerLogger for client
let ServerLogger = class {
  constructor() {
    throw new Error(
      `You are trying to use server logger inside client environment!
            Consider importing ClientLogger instead.`
    );
  }
};
export { ClientLogger, S2MServices, ServerLogger };
