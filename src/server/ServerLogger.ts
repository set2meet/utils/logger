import { Server as SocketServer, ServerOptions } from 'socket.io';
import BaseLogger, { ILoggerSettings } from '../common/BaseLogger';
import { StdOutTransport } from './transport/StdOutTransport';
import ServerSyncTransport from '../client/transport/ServerSyncTransport';
import * as os from 'os';
import { IAdditionalBackendFields, IAdditionalLogFields, ILogData, TLogObject } from '../common/types/ILogObject';
import { S2MServices } from '../common/types/S2MServices';
import { ILoggerInternals } from '../common/types/ILogger';
import { TransportEvent } from '../common/types/transport/TransportEvent';
import { BasicEnum } from '../common/types/BasicEnum';
import { TBoundFields } from '../common/types/TBoundFields';
import { DefaultModules } from '../common/types/DefaultModules';
import { SocketDisconnectReason } from '../common/types/transport/SocketDisconnectReason';
import JSONSerializer from '../common/serializers/JSONSerializer';

export type TServerFactory = (config: IBackendTransportConfig) => SocketServer;

export interface IBackendTransportConfig extends Partial<ServerOptions> {
  serverFactory?: TServerFactory;
  port: string;
}

const CONNECTION_EVENT = 'connection';

type TSystemInfo = {
  hostname: string;
  pid: string;
};

/**
 * @TODO:
 * 1. should handle auth
 * 2. should batch messages
 * 3. should handle connection issues
 */
export class ServerLogger<
  Module extends BasicEnum,
  ErrorCode extends BasicEnum,
  LogDataModel extends ILogData = ILogData,
  ReportEventType extends BasicEnum = BasicEnum,
  ReportEventModel extends ILogData = ILogData
> extends BaseLogger<Module, ErrorCode, LogDataModel, ReportEventType, ReportEventModel> {
  protected server: SocketServer;
  protected systemInfo: TSystemInfo;

  public static DEFAULT_SERVER_CONFIG: Partial<IBackendTransportConfig> = {
    cors: {
      origin: '*:*',
    },
    path: '/ws',
    pingTimeout: 3000,
    pingInterval: 3000,
  };

  constructor(
    protected service: S2MServices,
    protected module: Module | DefaultModules.DefaultModule = DefaultModules.DefaultModule,
    protected settings: ILoggerSettings = {},
    protected boundFields: TBoundFields<LogDataModel, ReportEventModel> = undefined,
    protected parent: ILoggerInternals<Module | DefaultModules.DefaultModule> = null
  ) {
    super(service, module, settings, boundFields, parent, new StdOutTransport());

    this.systemInfo = {
      hostname: os.hostname(),
      pid: process.pid.toString(),
    };
  }

  public startToSync(serverConfig: IBackendTransportConfig): void {
    const config: IBackendTransportConfig = {
      ...ServerLogger.DEFAULT_SERVER_CONFIG,
      ...serverConfig,
    };

    const { serverFactory, port } = serverConfig;

    this.server = serverFactory ? serverFactory(config) : new SocketServer(+port, config);
    this.server.on(CONNECTION_EVENT, (clientSocket) => {
      clientSocket.on(TransportEvent.LOG, (message) => {
        const backendTransport: StdOutTransport = this.transport as StdOutTransport;
        const serializer = new JSONSerializer();

        this.decompressLogDump(message)
          // @TODO: handle client timezone here too!
          .map((value) => {
            return (serializer.deserialize(value) as unknown) as TLogObject<any>;
          })
          .sort((a, b) => {
            const date1 = new Date(a.time).getMilliseconds();
            const date2 = new Date(b.time).getMilliseconds();
            if (date1 > date2) {
              return 1;
            }
            if (date1 < date2) {
              return -1;
            }
            return 0;
          })
          .forEach((value) => backendTransport.log(value));
      });
      const internalChildLogger = this.createBoundChild<{ reason: SocketDisconnectReason }>(
        DefaultModules.DefaultModule
      );
      clientSocket.on('error', (error) => {
        internalChildLogger.error(error, 'socket error');
      });
      clientSocket.on('disconnect', (reason: SocketDisconnectReason) => {
        switch (reason) {
          case SocketDisconnectReason.ServerDisconnect:
          case SocketDisconnectReason.V2ServerDisconnect:
            internalChildLogger.info('The socket was forcefully disconnected with socket.disconnect()', { reason });
            break;
          case SocketDisconnectReason.ClientDisconnect:
          case SocketDisconnectReason.V2ClientDisconnect:
            internalChildLogger.info('The client has manually disconnected the socket using socket.disconnect()', {
              reason,
            });
            break;
          case SocketDisconnectReason.PingTimeout:
            internalChildLogger.warn('The client did not respond in the pingTimeout range', { reason });
            break;
          case SocketDisconnectReason.TransportClose:
            internalChildLogger.warn(
              'The connection was closed (example: the user has lost connection, or the network was changed from WiFi to 4G)',
              { reason }
            );
            break;
          case SocketDisconnectReason.TransportError:
            internalChildLogger.warn('The connection has encountered an error', { reason });
            break;
          default:
            internalChildLogger.info('Socket disconnected', { reason });
            break;
        }
      });
    });
  }

  protected calculateAdditionalFields(): IAdditionalLogFields<Module> | IAdditionalBackendFields<Module> {
    return {
      ...this.systemInfo,
      ...super.calculateAdditionalFields(),
    };
  }

  protected decompressLogDump(logDump: string): string[] {
    // @TODO: implement decompression here
    return logDump.split(ServerSyncTransport.BATCH_SEPARATOR);
  }

  public destroy(): void {
    this.server.close();
    this.server = null;
  }
}
