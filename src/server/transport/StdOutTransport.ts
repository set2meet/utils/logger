import { ILogTransport } from '../../common/types/transport/ILogTransport';
import { ILogData, ILogObject } from '../../common/types/ILogObject';
import { BasicEnum } from '../../common/types/BasicEnum';
import JSONSerializer from '../../common/serializers/JSONSerializer';

export class StdOutTransport implements ILogTransport {
  public destroy(): void {}

  public rawLog(message: string): void {
    // @TODO: this is temp solution, need to get rid of how to handle it in bunyan (or remove it)
    this.log(new JSONSerializer().deserialize(message) as any);
  }

  public log<Modules extends BasicEnum, LogDataModel extends ILogData = ILogData>(
    message: ILogObject<Modules, LogDataModel>
  ): void {
    // this.bunyanLogger.info(message);
    console.log(message);
  }

  // tslint:disable-next-line:no-empty
  public setup(config?: {}): void {}
}
